import classNames from "classnames"

const variants = {
  reset: "",
  submit: "bg-blue-600 text-white"
}

const FormButton = ({ type, className, ...otherProps }) => (
  <button
    type={type}
    className={classNames(
      "py-3 px-4 rounded-md font-bold",
      variants[type],
      className
    )}
    {...otherProps}
  />
)

export default FormButton
