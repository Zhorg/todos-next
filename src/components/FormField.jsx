import classNames from "classnames"
import { useField } from "formik"

const FormField = ({ name, label, className, ...props }) => {
  const [field, meta] = useField(name)

  const hasError = meta.touched && meta.error

  return (
    <label className={classNames("flex flex-col gap-2", className)}>
      {label}
      <input
        {...field}
        {...props}
        className={classNames(
          "border border-gray-300 px-2 py-1 h-10 rounded-md",
          {
            "border-red-600": hasError
          }
        )}
      />
      {hasError ? (
        <span className="text-sm text-red-600 font-semibold">{meta.error}</span>
      ) : null}
    </label>
  )
}

export default FormField
