import { XMarkIcon } from "@heroicons/react/24/outline/"
import { Form, Formik } from "formik"
import * as yup from "yup"
import FormButton from "./FormButton"
import FormField from "./FormField"

const initialValues = {
  text: ""
}

const validationSchema = yup.object().shape({
  text: yup.string().required()
})

const TextInputModal = ({
  title,
  label,
  yesLabel,
  noLabel,
  close,
  onSubmit
}) => (
  <article className="fixed inset-0 bg-white">
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      validateOnBlur={false}
      onSubmit={onSubmit}
    >
      <Form className="flex-grow flex flex-col h-full">
        <header className="py-4 px-6 flex justify-between border-b border-gray-200">
          <h1 className="text-3xl font-bold">{title}</h1>
          <button type="reset" onClick={close}>
            <XMarkIcon className="w-6 h-6" />
          </button>
        </header>
        <FormField
          label={label}
          type="text"
          name="text"
          className="flex-grow p-6"
          autoFocus={true}
        />
        <menu className="flex justify-end gap-4 p-6">
          <FormButton type="reset" onClick={close}>
            {noLabel}
          </FormButton>
          <FormButton type="submit">{yesLabel}</FormButton>
        </menu>
      </Form>
    </Formik>
  </article>
)

export default TextInputModal
