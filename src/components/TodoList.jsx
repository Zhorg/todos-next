const { default: TodoListItem } = require("./TodoListItem")

const TodoList = ({ todos, setDone, handleEdit, handleDelete, filterDone }) => {
  const displayedTodos = filterDone
    ? Object.entries(todos).filter(([_todoId, { done }]) => !done)
    : Object.entries(todos)

  return (
    <ul className="w-full">
      {displayedTodos.map(([todoId, todo]) => (
        <TodoListItem
          key={todoId}
          id={todoId}
          description={todo.description}
          done={todo.done}
          setDone={setDone}
          handleEdit={handleEdit}
          handleDelete={handleDelete}
        />
      ))}
    </ul>
  )
}

export default TodoList
