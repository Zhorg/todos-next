import { TrashIcon } from "@heroicons/react/20/solid"
import { useCallback } from "react"

const TodoListItem = ({
  id,
  description,
  done,
  setDone,
  handleEdit,
  handleDelete
}) => (
  <li className="w-full flex justify-between items-center gap-4 h-11 px-6 border-b border-gray-300 select-none group">
    <input
      type="checkbox"
      title={`${done ? "Uncheck" : "Check"} "${description}"`}
      className="appearance-none w-5 h-5 border border-slate-600 checked:bg-green-500"
      checked={done}
      onChange={useCallback(() => setDone(id, !done), [id, setDone, done])}
    />
    <p
      className="flex-grow self-stretch font-cursive flex items-center"
      onClick={useCallback(() => handleEdit(id), [id, handleEdit])}
    >
      {description}
    </p>
    <TrashIcon
      className="h-5 w-5 invisible group-focus:visible group-hover:visible"
      onClick={useCallback(() => handleDelete(id), [id, handleDelete])}
    />
  </li>
)

export default TodoListItem
