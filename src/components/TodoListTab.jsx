import { useCallback } from "react"
import classNames from "classnames"

import { isEntirelyVisible } from "@/helpers/dom"

const TodoListTab = ({
  id,
  listName,
  doneCount,
  totalCount,
  switchTab,
  active
}) => {
  const handleClick = useCallback(() => switchTab(id), [id, switchTab])
  const scrollIfNeeded = useCallback((e) => {
    if (!isEntirelyVisible(e.currentTarget)) {
      e.target.scrollIntoView({ behavior: "smooth", inline: "center" })
    }
  }, [])

  const progress = totalCount > 0 ? (doneCount * 100) / totalCount : 0

  return (
    <li
      className="px-3 pt-2 border-t border-l first:border-l-0 last:border-r border-slate-300 rounded-t-lg relative"
      onClick={scrollIfNeeded}
    >
      <button
        type="button"
        title={listName}
        className="whitespace-nowrap"
        onClick={handleClick}
      >
        {listName}{" "}
        <span className="pr-2 rounded-xl bg-blue-400">
          <span className="px-2 rounded-xl bg-green-400">{doneCount}</span>{" "}
          {totalCount}
        </span>
      </button>
      <div
        className={classNames(
          "w-full h-1 bg-gray-400 absolute left-0 bottom-0",
          {
            invisible: !active
          }
        )}
      >
        <div
          className="bg-green-600 h-1 transition-[width] duration-500"
          style={{
            width: `${progress}%`
          }}
        ></div>
      </div>
    </li>
  )
}

export default TodoListTab
