import {
  PlusIcon,
  PencilSquareIcon,
  TrashIcon,
  CheckIcon,
  CheckCircleIcon
} from "@heroicons/react/20/solid"

const TodoListToolBar = ({
  addTodo,
  editTodoList,
  deleteTodoList,
  toggleFilterDone,
  filterDone,
  enabled
}) => (
  <menu className="box-content flex justify-between gap-2 px-6 py-2 border-y border-slate-300 h-5">
    <li>
      <button
        type="button"
        title="Add a To Do"
        onClick={addTodo}
        disabled={!enabled}
      >
        <PlusIcon className="w-5 h-5" />
      </button>
    </li>
    <li>
      <button
        type="button"
        title="Edit the To Do List"
        onClick={editTodoList}
        disabled={!enabled}
      >
        <PencilSquareIcon className="w-5 h-5" />
      </button>
    </li>
    <li>
      <button
        type="button"
        title="Delete the To Do List"
        onClick={deleteTodoList}
        disabled={!enabled}
      >
        <TrashIcon className="w-5 h-5" />
      </button>
    </li>
    <li className="flex-grow"></li>
    <li>
      <button
        type="button"
        title={filterDone ? "Show all To Dos" : "Show only unfinished To Dos"}
        onClick={toggleFilterDone}
      >
        {filterDone ? (
          <CheckIcon className="w-5 h-5" />
        ) : (
          <CheckCircleIcon className="w-5 h-5" />
        )}
      </button>
    </li>
  </menu>
)

export default TodoListToolBar
