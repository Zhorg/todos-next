import { PlusIcon } from "@heroicons/react/24/solid"

import TodoListTab from "@/components/TodoListTab"

import * as todosHelpers from "@/helpers/todos"

const TodoListsTabBar = ({ todoLists, switchTab, newTodoList, current }) => (
  <nav className="flex overflow-x-auto scrollbar-hide">
    <ul className="flex">
      {Object.entries(todoLists).map(([todoListId, todoList]) => {
        const { done, total } = todosHelpers.count(todoList)

        return (
          <TodoListTab
            key={todoListId}
            id={todoListId}
            listName={todoList.name}
            switchTab={switchTab}
            doneCount={done}
            totalCount={total}
            active={current === todoListId}
          />
        )
      })}
    </ul>
    <button
      type="button"
      className="ml-4 py-2 px-3 border border-b-0 border-slate-300 rounded-t-lg"
      title="Add a To Do List"
      onClick={newTodoList}
    >
      <PlusIcon className="h-6 w-6" />
    </button>
  </nav>
)

export default TodoListsTabBar
