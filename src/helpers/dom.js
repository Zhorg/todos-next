export const isEntirelyVisible = (element) => {
  const boundingRect = element.getBoundingClientRect()

  return boundingRect.left >= 0 && boundingRect.right <= window.innerWidth
}
