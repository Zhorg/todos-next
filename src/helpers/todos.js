export const count = (todoList) => {
  const todos = Object.values(todoList.todos)

  const total = todos.length
  const done = todos.filter((todo) => todo.done).length
  const notDone = total - done

  return {
    total,
    done,
    notDone
  }
}
