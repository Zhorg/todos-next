const initialState = {
  focused: "3",
  filterDone: false,
  lastTodoListId: 3,
  todoLists: {
    1: {
      id: 1,
      name: "A normal day",
      lastTodoId: 7,
      todos: {
        1: { description: "Wake up", done: false },
        2: { description: "Eat breakfast", done: false },
        3: { description: "Wash yourself", done: false },
        4: { description: "Go to work", done: false },
        5: { description: "Go back", done: false },
        6: { description: "Eat dinner", done: false },
        7: { description: "Sleep", done: false }
      }
    },
    2: {
      id: 2,
      name: "Homework",
      lastTodoId: 3,
      todos: {
        1: { description: "HTML / CSS / JS", done: true },
        2: { description: "React", done: true },
        3: { description: "Next.js", done: false }
      }
    },
    3: {
      id: 3,
      name: "To Do List features",
      lastTodoId: 8,
      todos: {
        1: { description: "Create a To Do List", done: true },
        2: { description: "Edit a To Do List", done: true },
        3: { description: "Delete a To Do List", done: true },
        4: { description: "Create a To Do", done: true },
        5: { description: "Edit a To Do", done: true },
        6: { description: "Delete a To Do", done: true },
        7: { description: "Mark a To Do as done", done: true },
        8: { description: "Show current list progress", done: false }
      }
    }
  }
}

export default initialState
