import { useCallback, useState } from "react"

// import initialState from "@/initialState"

import TodoList from "@/components/TodoList"
import TodoListToolBar from "@/components/TodoListToolBar"

import deepmerge from "deepmerge"
import TodoListsTabBar from "../components/TodoListsTabBar"
import TextInputModal from "@/components/TextInputModal"

const TodosPage = () => {
  const [filterDone, setFilterDone] = useState(false)
  const [focusedTab, setFocusedTab] = useState(null)
  const [lastTodoListId, setLastTodoListId] = useState(0)
  const [todoLists, setTodoLists] = useState({})
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [modal, setModal] = useState({
    title: "",
    label: "",
    yesLabel: "",
    noLabel: "",
    onSubmit: () => {},
    close: () => {}
  })

  const updateTodoLists = useCallback(
    (update) =>
      setTodoLists((currentState) =>
        typeof update === "function"
          ? update(currentState)
          : deepmerge(currentState, update)
      ),
    []
  )

  const createTodoList = useCallback(
    (name) => {
      const todoListId = lastTodoListId + 1

      setFocusedTab(todoListId.toString())
      setLastTodoListId(todoListId)
      updateTodoLists({
        [todoListId]: {
          name,
          lastTodoId: 0,
          todos: {}
        }
      })
    },
    [lastTodoListId, updateTodoLists]
  )

  const updateTodoListName = useCallback(
    (newName) =>
      updateTodoLists({
        [focusedTab]: {
          name: newName
        }
      }),
    [focusedTab, updateTodoLists]
  )

  const deleteTodoList = useCallback(() => {
    const todoListIds = Object.keys(todoLists)

    const todoListIndex = todoListIds.indexOf(focusedTab)

    const newFocusedTab =
      todoListIndex === 0
        ? todoListIds.at(1)
        : todoListIds.at(todoListIndex - 1)

    setFocusedTab(newFocusedTab)

    updateTodoLists((todoLists) => {
      const { [focusedTab]: deletedTodoList, ...otherTodoLists } = todoLists

      return { ...otherTodoLists }
    })
  }, [focusedTab, todoLists, updateTodoLists])

  const toggleFilterDone = useCallback(
    () => setFilterDone((filterDone) => !filterDone),
    []
  )

  const createTodo = useCallback(
    (description) => {
      const { lastTodoId } = todoLists[focusedTab]

      const todoId = lastTodoId + 1

      updateTodoLists({
        [focusedTab]: {
          lastTodoId: todoId,
          todos: {
            [todoId]: {
              id: todoId,
              description,
              done: false
            }
          }
        }
      })
    },
    [focusedTab, todoLists, updateTodoLists]
  )

  const setTodoDone = useCallback(
    (todoId, done) =>
      updateTodoLists({
        [focusedTab]: {
          todos: {
            [todoId]: {
              done
            }
          }
        }
      }),
    [focusedTab, updateTodoLists]
  )

  const updateTodoDescription = useCallback(
    (todoId, description) =>
      updateTodoLists({
        [focusedTab]: {
          todos: {
            [todoId]: { description }
          }
        }
      }),
    [focusedTab, updateTodoLists]
  )

  const deleteTodo = useCallback(
    (todoId) =>
      updateTodoLists((todoLists) => {
        const {
          todos: { [todoId]: deletedTodo, ...otherTodos }
        } = todoLists[focusedTab]

        return {
          ...todoLists,
          [focusedTab]: {
            ...todoLists[focusedTab],
            todos: {
              ...otherTodos
            }
          }
        }
      }),
    [focusedTab, updateTodoLists]
  )

  const closeModal = useCallback(() => setIsModalVisible(false), [])

  const onSubmit = useCallback(
    (handler) =>
      ({ text }, { resetForm }) => {
        handler(text)
        resetForm({
          text: ""
        })
        closeModal()
      },
    [closeModal]
  )

  const addTodoList = useCallback(() => {
    setModal({
      title: "Create a new list",
      label: "Description",
      yesLabel: "Create",
      noLabel: "Cancel",
      close: closeModal,
      onSubmit: onSubmit(createTodoList)
    })
    setIsModalVisible(true)
  }, [onSubmit, createTodoList, closeModal])

  // if (Object.keys(todoLists).length === 0 && !isModalVisible) {
  //   addTodoList()
  // }

  const editTodoList = useCallback(
    (todoListId) => {
      setModal({
        title: "Edit list",
        label: "Description",
        yesLabel: "Save",
        noLabel: "Cancel",
        close: closeModal,
        onSubmit: onSubmit(updateTodoListName.bind(null, todoListId))
      })
      setIsModalVisible(true)
    },
    [onSubmit, updateTodoListName, closeModal]
  )

  const addTodo = useCallback(() => {
    setModal({
      title: "Add todo",
      label: "Description",
      yesLabel: "Create",
      noLabel: "Cancel",
      close: closeModal,
      onSubmit: onSubmit(createTodo)
    })
    setIsModalVisible(true)
  }, [createTodo, onSubmit, closeModal])

  const editTodo = useCallback(
    (todoId) => {
      setModal({
        title: "Edit todo",
        label: "Description",
        yesLabel: "Save",
        noLabel: "Cancel",
        close: closeModal,
        onSubmit: onSubmit(updateTodoDescription.bind(null, todoId))
      })
      setIsModalVisible(true)
    },
    [updateTodoDescription, onSubmit, closeModal]
  )

  return (
    <main className="mt-8 select-none">
      <section>
        <TodoListsTabBar
          todoLists={todoLists}
          newTodoList={addTodoList}
          switchTab={setFocusedTab}
          current={focusedTab}
        />
        <TodoListToolBar
          addTodo={addTodo}
          editTodoList={editTodoList}
          deleteTodoList={deleteTodoList}
          toggleFilterDone={toggleFilterDone}
          filterDone={filterDone}
          enabled={focusedTab !== null}
        />
        {focusedTab && (
          <TodoList
            todos={todoLists[focusedTab].todos}
            setDone={setTodoDone}
            handleEdit={editTodo}
            handleDelete={deleteTodo}
            filterDone={filterDone}
          />
        )}
      </section>
      <section>{isModalVisible && <TextInputModal {...modal} />}</section>
    </main>
  )
}

export default TodosPage
